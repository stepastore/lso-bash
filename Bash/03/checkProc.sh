#!/bin/sh

# Si realizzi uno script di shell che accetti come parametri un intero ed una stringa
# che indicano, rispettivamente, PID e username. Lo script dovrà controllare se
# esiste in esecuzione un processo con il PID e lo username indicato. Se tale
# processo esiste verranno stampati nel file processi.txt i PID di tutti i processi
# “antenati” (padre, padre del padre etc.).

if [ "$#" -lt "2" ]; then
	echo "Error: can't work with no input mate!" 2>&1
	echo "Usage: ./checkProc.sh <pid> <username>"
	exit 1
elif [ "$#" -gt "2" ]; then
	echo "Warning: the exceeding parameters will be ignored."
fi

proc=$(ps -u $2 2> /dev/null | wc -l)

# Controllo se esisono processi in esecuzione per l'utente specificato
if [ $proc -gt "0" ]; then
	
	# Controllo se esiste un pid associato all'utente
	proc=$(ps -u $2 | grep "[[:space:]]*$1" | wc -l)

	# Se ve ne sono
	if [ $proc -gt "0" ]; then
		
	else
		echo "No process matches pid: $1."
		exit 3
	fi	


else
	echo "No user $2 found."
	exit 2
fi

