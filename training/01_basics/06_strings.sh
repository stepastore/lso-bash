#!/bin/bash

# Concatenate strings is as easy as putting the variables one next to the other.
# No operators involved. Sweet. :)
a="bisc"
b="cuit"
fruit="apple banana banana banana cherry"
c=$a$b
echo $c

# ----- STRING LENGTH

# In order to get the length of a string let's use the expansion operator
# preceded by a pound sign:

echo "How many letters has the word $c?"
echo "$c has ${#c} letters."


# ----- SUBSTRINGS

# it extracts the substring taking letters after the 3rd from
# the beginning of the string:
d=${c:3}
echo $d

# it extracts 2 letters after the 3rd from the beginning of the string:
d=${c:3:2}
echo $d

# it extracts the last 3 letters from the string:
d=${c: -3} #the space before the - is needed
echo $d

# it extracts the first 2 letters from the final 3
d=${c: -3:2}
echo $d

# ----- SUBSTITUTION
echo "Fruit available: $fruit"

# Changing the first banana with strawberry
echo "Fruit available: ${fruit/banana/strawberry}"

# Changing all bananas with strawberry
echo "Fruit available: ${fruit//banana/strawberry}"

# Changing a word only if it is at the beginning of the string
echo "Fruit available: ${fruit/#apple/watermelon}"
echo "Fruit available: ${fruit/#banana/watermelon}"

#Changing a word only if it is at the end of the string
echo "Fruit available: ${fruit/%cherry/watermelon}"
echo "Fruit available: ${fruit/%banana/watermelon}"
