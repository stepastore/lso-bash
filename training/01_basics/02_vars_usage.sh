#! /bin/bash

# Declaration MUST NOT contain any spaces or execution will result in errors.
a=Hello

# When there are spaces in a string, you need to surround the string with 
# double quotes.
b="Good Morning"
c=16

# ----------- ADVANCED DECLARATION
declare -i d=123 #variable d is declared as an integer
declare -r e=456 #variable e is read-only
declare -l f="STRing" #content of the string is converted to lowercase
declare -u g="STRing" #content of the string is converted to uppercase

echo $a
echo $b
echo $c

echo "$b! I have $c apples."
echo "$d days have passed since my $e daughters left me. No $f, and I repeat, no $g has been given to me, to date."
