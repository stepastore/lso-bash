#!/bin/bash

# Puts some text into a file and overrides all the content.
echo "Hello World!" > file.txt

# Appends some text to a file.
echo "Hello People!" >> file.txt

# Reads lines from a file one by one
i=1
while read line; do
	echo "Line $i: $line"
	((i++))
done < file.txt

# Instead of echoing out a long piece of text, you can use the heredoc.
# Here it is how it works:
# 1. choose a string which serves as delimiter
# 2. write your text
# 3. close your text with the delimiter.
# NOTE: the word MUST BE unique enough not to appear in the text.

cat << MY_HEREDOC_DELIMITER
Suddenly something
has happened to me
as I was having my
cup of tea.
MY_HEREDOC_DELIMITER
