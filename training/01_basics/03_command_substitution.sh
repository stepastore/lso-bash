#!/bin/bash

# Should a BASH command be needed in your script, you will need to use substitution
# in order to make it work properly.

ping_time=$(ping -c 1 foglioinbianco.net | grep -e "bytes from" | cut -d = -f 4)
echo "foglioinbianco.net ha risposto in $ping_time."
