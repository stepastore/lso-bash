#!/bin/bash

# Math operations are interpreted with $(( expression ))

# BASH supports the classical ops (addition, subtraction, 
# multiplication, division, modulo) and exponentiation (**).

op1=2
op2=4

ans=$((2**4))
echo "The result is $ans."

# Shortcut for adding or subtracting a unit are also availabile

e=1
echo "e: $e"
((e++))
echo "e: $e"
((e--))
echo "e: $e"

# Shortcuts for the standard operations
((e+=5))
echo "e: $e"
((e-=5))
echo "e: $e"
((e*=5))
echo "e: $e"
((e/=5))
echo "e: $e"

# It is also worth noting, BASH handles integers only. If you need to work with
# floats, you need to use the bc command:
f=$(echo 1/3 | bc -l)
echo "1/3 = $f"
