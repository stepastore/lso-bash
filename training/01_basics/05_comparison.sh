#!/bin/bash

# To incorporate logic use [[ expression ]], keeping in mind 0 = true and 1 is false.
# Operators for comparison are the same as the ones in C.

cat="cat"
dog="dog"

[[ $cat == $cat ]]
echo "Is $cat equal to $cat? $?"

[[ $cat == $dog ]]
echo "Is $cat equal to $dog? $?"

# This comparison will lead to TRUE. The reason is  it is performed lexically rather
# than numerically.
[[ 20 > 1000 ]]
echo "Is 20 greater than 1000? $?"

# In order to compare two values as numbers, these are the operators
# you need to use: -gt -lt -ge -le -eq -ne
[[ 20 -gt 1000 ]]
echo "Is 20 greater than 1000? $?"

# Logic operators are && as AND, || as  OR and ! as NOT.
# Ex. [[ $a && $b ]]

# Moreover -z tests for a null value and -n for a not null value
[[ -z void ]]
echo "Is void null? $?"

[[ -n dog ]]
echo "Is dog not null? $?"
