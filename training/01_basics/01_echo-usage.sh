#! /bin/bash

# This is a bash comment.

greeting="hello"

# Special characters such as ( and ) must be escaped before use.

# With no quotes everything is interpretated. That is why we need to escape
# the special characters.
echo $greeting, world \(planet\)!

# Single quotes will display everything literally. No interpretation whatsoever.
echo '$greeting, world (planet)!'

# Double quotes will interpret what needs to be interpreted and stands in
# the middle of the previous two.

# Note: no escape char is needed for the parenthesis!
echo "$greeting, world (planet)!"
