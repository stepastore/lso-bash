/*
Scrivere un programma che prenda come parametri un file in input ed una stringa. Il programma deve 
creare un processo figlio, che lanci il programma 'scriviStringa' mediante la primitiva 'execlp' per
scrivere la stringa nel file in input. Il processo padre aspetta il figlio e stampa a video il conte-
nuto del file in input.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

#define BUFFER_SIZE 64


int main(int argc, char *argv[])
{
	pid_t pid;
	int fd;
	int status;
	int err;
	char *buffer;

	if (argc < 3)
	{
		fprintf(stderr, "Execution failed. Not enough arguments, dude. C'mon!\n");
		fprintf(stdout, "Usage: ./putandoutput <input file> <string>\n");
		exit(1);
	}
	else if (argc > 3)
	{
		fprintf(stdout, "Didn't ask for your life story. Exceeding input will be ignored.\nMoving on...\n");
	}

	pid = fork();

	if (pid == 0)
	{
		if (execlp("./stringinfile", "stringinfile", argv[1], argv[2], (char *)0) < 0)
		{
			exit(3);
		}
	}
	if (pid > 0)
	{
		wait(&status);
		if (WIFEXITED(status))
		{
			fd = open(argv[1], O_RDONLY);
			if (fd < 0)
			{
				fprintf(stderr, "Can't open the file. Maybe the lock is broken. Gotta call the blacksmith, pal!\n");
				exit(5);
			}
			buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
			while (read(fd, buffer, BUFFER_SIZE) > 0)
			{
				write(STDOUT_FILENO, buffer, BUFFER_SIZE);
			}
			free(buffer);
		}
		else
		{
			fprintf(stderr, "Urgent: Child was killed -- I repeat -- child was killed.\n");
			exit(4);
		}
	}
	else
	{
		fprintf(stderr, "Can't create a new process. Can't do this, can't do that. I hate my life cycle.\n");
		exit(2);
	}

	return 0;
}