#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>


int main(int argc, char *argv[])
{
	int string_size;
	int out_fd;

	if (argc < 3)
	{
		fprintf(stderr, "Execution failed. Not enough arguments, dude. C'mon!\n");
		fprintf(stdout, "Usage: ./stringinfile <input file> <string>\n");
		exit(1);
	}
	else if (argc > 3)
	{
		fprintf(stdout, "Didn't ask for your life story. Exceeding input will be ignored.\nMoving on...\n");
	}

	out_fd = open(argv[1], O_WRONLY|O_CREAT|O_TRUNC, S_IRWXU|S_IRWXG|S_IRWXO);

	if (out_fd < 0)
	{
		fprintf(stderr, "Couldn't open the file. Why can't I do anything right?!?\n");
		exit(2);
	}

	string_size = strlen(argv[2]);
	if (write(out_fd, argv[2], string_size) < 0)
	{
		fprintf(stderr, "Some error occurred while writing. So many distractions.\n");
	}

	if (close(out_fd) < 0)
	{
		fprintf(stderr, "I'm sorry, I can't close the file. You exchanged my keys, didn't ya?\n");
	}
	return 0;
}