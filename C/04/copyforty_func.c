#include "copyforty_func.h"

int getcharsfromint(const int n, const int base)
{
	int integer = n;
	int i = 0;
	while (integer > 0)
	{
		integer = integer / base;
		i++;
	}
	return i;
}

int writeattheend(const int fd, const int pid, const int base)
{
	char *buffer;
	int buffer_size = getcharsfromint(pid, base);
	buffer = (char *) malloc (sizeof(char) * buffer_size);
	sprintf(buffer, " %d", pid);
	return (write(fd, buffer, buffer_size));
}