/*
Scrivere un programma C che prenda come parametri due nomi di file. Il programma dovrà 
generare un processo figlio, che aprirà uno dei due file in input, copiando i primi 40
caratteri nel file in output, per aggiungere poi in coda il proprio pid. Il padre atten-
derà la terminazione del figlio ed aggiungerà il proprio pid.
*/

#include "copyforty_func.h"


int main(int argc, char *argv[])
{
	int in_fd;
	int out_fd;
	int status;
	int pid;
	int written_bytes;
	int read_bytes;
	int buffer_size;
	char *buffer_1;
	char *buffer_2;

	if (argc < 3)
	{
		perror("Numero di parametri in ingresso errato.\n");
		printf("Utilizzo: ./copyforty <input file> <output file>");
		exit(1);
	}
	else if (argc > 3)
	{
		printf("Attenzione: numero di parametri in ingresso errato. Gli input in eccedenza saranno ignorati.\n");
	}

	out_fd = open(argv[2], O_WRONLY|O_TRUNC|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO);
	if (out_fd < 0)
	{
		perror("Impossibile aprire il file.\n");
		exit(1);
	}

	pid = fork();

	if (pid == 0)
	{
		buffer_1 = (char *)malloc(sizeof(char) * 40);
		
		in_fd = open(argv[1], O_RDONLY);
		if (in_fd < 0)
		{
			perror("Impossibile aprire il file.\n");
		}

		read(in_fd, buffer_1, 40);
		write(out_fd, buffer_1, 40);

		written_bytes = writeattheend(out_fd, getpid(), 10);
		if (written_bytes < 0)
		{
			perror("Errore nella scrittura del file.\n");
		}
		free(buffer_1);
		close(in_fd);
	}
	else if (pid > 0)
	{
		wait(&status);
		written_bytes = writeattheend(out_fd, getpid(), 10);
		if (written_bytes < 0)
		{
			perror("Errore nella scrittura del file.\n");
		}
	}
	else
	{
		perror("Impossibile generare processo figlio.");
	}
	
	close(out_fd);
	return 0;
}