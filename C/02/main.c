/*
	Si realizzi un programma in che possa ricevere come input
	un numero intero immesso da tastiera. A tale numero, il processo
	figlio creato somma 15, mentre il processo padre somma 10.
*/

#include <stdio.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	int pid;
	int n;
	printf("Inserire un intero.\nIntero: ");
	scanf("%d", &n);

	pid = fork();

	if (pid == 0)
	{
		printf("Sono il figlio e ho pid %d. Il mio risultato è: %d.\n", getpid(), n+15);
	}
	else if (pid > 0)
	{
		printf("Generato un figlio con pid %d...\n", pid);
		printf("Sono il padre e ho pid %d. Il mio risultato è %d.\n", getpid(), n+10);
	}
	else
	{
		printf("Errore nella chiamata a fork()");
		exit(1);
	}

	return 0;
}