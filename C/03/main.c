/*
	Scrivere un programma C che prenda come parametro in ingresso un file "input_file.in". Il programma
	dovrà:
		- controllare che il numero di argomenti passati sia corretto;
		- aprire tale file e creare un processo figlio che scrive nel file in input una stringa
		  inserita da tastiera;
		- il padre deve attendere la terminazione del figlio e stampare a video il contenuto di "input_file.in".
*/

#include "waitforme_func.h"

int main(int argc, char *argv[])
{
	int fd;
	int pid;
	int read_bytes = 0;
	int returned_pid = 0;
	int status;
	int byte_count;
	char *buffer;

	if (argc < 3)
	{
		printf("Numero di parametri errato.\n");
		printf("Utilizzo: ./waitforme <nome file> <stringa>");
		exit(1);
	}
	else if (argc > 3)
	{
		printf("Sono stati inseriti parametri aggiuntivi. Essi saranno ignorati.\n");
	}

	fd = open(argv[1], O_RDWR|O_CREAT|O_TRUNC, S_IRWXU|S_IRWXG|S_IRWXO);
	if (fd < 0)
	{
		perror("Errore nell'apertura del file in input.");
	}

	pid = fork();

	if (pid == 0)
	{
		printf("Sono il figlio. Mio pid: %d.\n", getpid());
		byte_count = count_chars(argv[2]);

		if (write(fd, argv[2], byte_count) > 0)
		{
			printf("Scrittura avvenuta correttamente.\n");
		}
		else
		{
			printf("Errore in scrittura. Errore: \n", errno);
		}
		status = 1;
	}
	else if (pid > 0)
	{
		returned_pid = wait(&status);
		lseek(fd, 0, SEEK_SET);
		printf("Sono il padre. Mio pid: %d. Mio figlio pid: %d\n", getpid(), pid);
		printf("Pid ritornato: %d\n", returned_pid);

		buffer = (char *)malloc(sizeof(char) * byte_count);
		
		while (read_bytes = read(fd, buffer, byte_count) > 0);

		printf("Stringa: %s\n", buffer);
		
	}
	else
	{
		perror("Errore nell'invocazione della fork()");
	}

	close(fd);

	return 0;
}