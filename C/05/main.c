/*
	Scrivere un programma C che prenda come parametri due nomi di file. Il programma deve creare un 
	processo figlio, che apre il file in input e scrive i primi e gli ultimi 10 caratteri nel file 
	di output. Il padre deve attendere il figlio e visualizzare il contenuto del file in output.
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int in_fd;
	int out_fd;
	int pid;
	int status;
	int cursor_position = 0;
	char *buffer;


	if (argc < 3)
	{
		fprintf(stderr, "Errore: numero di parametri errato.\n");
		printf("Utilizzo: ./firstlast10 <file input> <file output>\n");
		exit(-1);
	}
	else if (argc > 3)
	{
		fprintf(stdout, "Attenzione: numero di parametri errato. Gli input in eccedenza saranno ignorati.\n");
	}

	out_fd = open(argv[2], O_CREAT|O_RDWR|O_TRUNC, S_IRWXU|S_IRWXG|S_IRWXO);
	if (out_fd < 0)
	{
		fprintf(stderr, "Errore nell'apertura del file. Errore no. %d.\n", errno);
		exit(errno); 
	}

	pid = fork();

	if (pid == 0)
	{
		buffer = (char *) malloc (sizeof(char) * 10);

		in_fd = open(argv[1], O_RDONLY);
		if (in_fd < 0)
		{
			fprintf(stderr, "Errore nell'apertura del file. Errore no. %d.\n", errno);
			exit(errno); 
		}

		// Read 10 characters from the start.
		read(in_fd, buffer, 10);

		// Write the 10 characters.
		write(out_fd, buffer, 10);

		// Go to 10 characters before the end.
		lseek(in_fd, -11, SEEK_END);

		// Read last 10 characters.
		read(in_fd, buffer, 10);

		// Write the 10 characters.
		write(out_fd, buffer, 10);

		if (close(in_fd) < 0)
		{
			fprintf(stderr, "Errore nella chiusura del file. Errore no. %d.", errno);
			exit(errno);
		}

		free(buffer);
	}
	else if (pid > 0)
	{
		wait(&status);
		buffer = (char *)malloc(sizeof(char) * 20);
		lseek(out_fd, -20, SEEK_END);
		read(out_fd, buffer, 20);
		write(STDOUT_FILENO, buffer, 20);
		fprintf(stdout, "\n");
		free(buffer);
	}
	else
	{
		fprintf(stderr, "Errore nella creazione del processo figlio. Errore no. %d.\n", errno);
		exit(errno);
	}


	if (close(out_fd) < 0)
	{
		fprintf(stderr, "Errore nella chiusura del file. Errore no. %d.", errno);
		exit(errno);
	}

	return 0;
}