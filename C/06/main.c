/*
	Scrivere un programma che prenda come parametri due file. Il programma deve creare un 
	processo figlio, che lancia il comando 'cp' mediante la primitiva 'execl' per copiare 
	il contenuto di un file nell'altro. Il processo padre aspetta il figlio e stampa a vi-
	deo il contenuto del file copiato.
*/

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#define BUFFER_SIZE 512

int main(int argc, char *argv[])
{
	pid_t pid;
	int out_fd;
	int status;
	char *buffer;

	if (argc < 3)
	{
		fprintf(stderr, "Errore: impossibile eseguire il programma. Numero di parametri errato.\n");
		fprintf(stdout, "Utilizzo ./mycopy <file input> <file output>\n");
		exit(1);
	}
	if (argc > 3)
	{
		fprintf(stdout, "Attenzione: numero di parametri errato. Il programma ignorerà i parametri in eccedenza.\n");
	}

	errno = 0;
	pid = fork();

	if (pid == 0)
	{
		if (execl("/bin/cp", "cp", argv[1], argv[2], (char *)0) < 0)
		{
			fprintf(stderr, "Impossibile procedere con l'esecuzione del processo figlio.\n");
			exit(3);
		}
	} 
	else if (pid > 0)
	{
		wait(&status);

		if (WIFEXITED(status))
		{
			printf("Il figlio ha terminato con status %d.\n", WEXITSTATUS(status));
			out_fd = open(argv[2], O_RDONLY);
			if (out_fd < 0)
			{
				fprintf(stderr, "Impossibile aprire il file %s.", argv[2]);
				exit(3);
			}

			buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
			
			while (read(out_fd, buffer, BUFFER_SIZE) > 0)
			{
				write(STDOUT_FILENO, buffer, BUFFER_SIZE);
			}
			free(buffer);

			if (close(out_fd) < 0)
			{
				fprintf(stderr, "Impossibile chiudere il file %s.", argv[2]);
			}
		}
		else
		{
			fprintf(stderr, "Terminazione non corretta del processo figlio.\n");
		}
	}
	else
	{
		fprintf(stderr, "Impossibile creare un nuovo processo. Errore %d.\n", errno);
		exit(2);
	}

	return 0;
}