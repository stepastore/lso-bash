/*
	Si realizzi un programma C che stampi a video il pid del processo
	che lancerà un fork() e che dopo la sua esecuzione sia in grado di
	identificare i processi genitore e figlio con i relativi pid, 
	testando i valori di ritorno da fork.
*/

	#include <stdio.h>
	#include <sys/types.h>

	int main(int argc, char *argv[])
	{
		int pid;
		printf("Sono il processo padre con pid %d e sto per lanciare la fork().\n", getpid());

		// Lancio la fork()
		pid = fork();

		// Se sono il processo figlio
		if (pid == 0)
		{
			printf("Figlio generato correttamente con pid: %d.\n", getpid());
		}
		// Sono il processo padre
		else if (pid > 0)
		{
			printf("Sono il padre e mio figlio ha pid %d.\n", pid);
		}
		else
		{
			printf("Errore nella chiamata a fork()\n");
			exit(1);
		}


		return 0;
	}